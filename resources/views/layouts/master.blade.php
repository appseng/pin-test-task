<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Products - @yield('title')</title>
    <style>
        .container {
            text-align: center;
            margin: auto;
            width: 50%;
            border: 3px solid green;
            padding: 10px;
        }
        .container table {
            width: 100%;
        }
        tr:nth-child(odd) { background-color: lightgray; }
        tr:hover { background-color: lightgreen; }
        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>

<div class="container">
    @yield('container')
</div>

</body>
</html>