@extends('layouts.master')

@section('title')
    Create
@endsection

@section('container')
    <h1>Create Product</h1>
    {{ Form::open(['url' => '/products']) }}
    <table>
        <tr>
            <th>Art</th>
            <th>Name</th>
            <th></th>
        </tr>
        <tr>
            <td>
                {{ Form::text('art') }}</td>
            <td>
                {{ Form::text('name') }}</td>
            <td>
                {{ Form::submit('Create') }}
            </td>
        </tr>
    </table>
    {{ Form::close() }}
@endsection