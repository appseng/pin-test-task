@extends('layouts.master')

@section('title')
    Show
@endsection

@section('container')
    <h1>Show Product</h1>
    <table>
        <tr>
            <th>Art</th>
            <th>Name</th>
            <th></th>
        </tr>
        <tr>
            <td>{{ $product->art }}</td>
            <td>{{ $product->name }}</td>
            <td>
                {{ link_to('/products/' . $product->id . '/edit', 'Edit') }}
            </td>
        </tr>
    </table>
@endsection
