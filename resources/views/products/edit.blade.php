@extends('layouts.master')

@section('title')
    Edit
@endsection
<style>
    .edit_form div {
        float: left;
        margin: 3px;
    }
    .container {
        height: 130px;
    }
    .container .edit_form {
        width: 460px;
        margin: auto;
        padding: 10px;
    }
</style>
@section('container')
    <h1>Edit Product</h1>
    <div class="edit_form">
        <div>
            {{ link_to('/products', 'Back') }}
        </div>
        <div>
            {{ Form::open(['url' => '/products/'.$product->id]) }}
            {{ method_field('PUT') }}
            {{ Form::text('art', $product->art) }}
            {{ Form::text('name', $product->name) }}
            {{ Form::submit('Edit') }}
            {{ Form::close() }}
        </div>
        <div>
            {{ Form::open(['url' => '/products/'.$product->id]) }}
            {{ method_field('DELETE') }}
            {{ Form::submit('Delete') }}
            {{ Form::close() }}
        </div>
    </div>
@endsection