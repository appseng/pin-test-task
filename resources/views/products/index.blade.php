@extends('layouts.master')

@section('title')
    Index
@endsection

@section('container')
    <h1>Index of Products</h1>
    {{ link_to('/products/create', 'Create') }}
    <table>
        <tr>
            <th>Art</th>
            <th>Name</th>
            <th></th>
        </tr>
    @foreach ($products as $product)
        <tr>
            <td>{{ $product->art }}</td>
            <td>{{ $product->name }}</td>
            <td>
                {{ link_to('/products/' . $product->id, 'Show') }}
                {{ link_to('/products/' . $product->id . '/edit', 'Edit') }}
            </td>
        </tr>
    @endforeach
    </table>

@endsection