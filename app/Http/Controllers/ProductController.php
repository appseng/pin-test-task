<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use \Illuminate\Database\QueryException;
use App\Http\Requests\CreateEditProduct;

class ProductController extends Controller
{
    public $restful = true;

    public function index(){
        $products = Product::all();
        return view('products.index', [
            'products' => $products,
        ]);
    }

    public function show($id){
        $product = Product::find($id);
        return view('products.show', [
            'product' => $product,
        ]);
    }

    public function edit($id) {
        $product = Product::find($id);
        return view('products.edit', [
            'product' => $product,
        ]);
    }

    public function store(CreateEditProduct $request)
    {
        $product = new Product;
        $product->name = $request->name;
        $product->art = $request->art;
        $product->save();
        return redirect('/products');
    }

    public function create() {
        return view('products.create');
    }

    public function update(CreateEditProduct $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->name;
        $product->art = $request->art;
        try {
            $product->save();
        } catch (QueryException $e) {
        }
        return redirect('/products');
    }

    public function destroy($id) {
        $product = Product::find($id);
        $product->delete();
        return redirect('/products');
    }
}